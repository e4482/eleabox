#.SILENT:
.PHONY: help update build

.DEFAULT_GOAL = help

COLOR_INFO    = \033[0;36m
COLOR_SUCCESS = \033[0;32m
COLOR_WARNING = \033[0;33m
COLOR_DANGER  = \033[0;31m
COLOR_DEFAULT = \033[m

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

update: ## Mettre à jour la branche master à partir du dépôt Moodlebox
	@echo "$(COLOR_INFO)Mise à jour de la branche master à partir du dépôt Moodlebox...$(COLOR_DEFAULT)"
	if [ -z `git remote | grep moodlebox` ] ; \
        then git remote add moodlebox https://github.com/moodlebox/moodlebox.git ; fi
	git fetch moodlebox master
	git checkout master
	git checkout -- hosts.yml
	git rebase moodlebox/master
	@echo "$(COLOR_WARNING)La branche master a été rebaser, pensez à forcer les modifications !$(COLOR_DEFAULT)"

build: ## Créer une Éléabox avec Ansible sur un Raspberry Pi
	@echo "$(COLOR_INFO)Mise en place de votre clé SSH...$(COLOR_DEFAULT)"
	if [ ! -d keys ] ; then mkdir keys ; fi
	cp ~/.ssh/id_rsa.pub keys/
	@echo "$(COLOR_INFO)Paramétrage de l'IP du Raspberry Pi...$(COLOR_DEFAULT)"
	raspi=`cat hosts.yml | grep ansible_host | sed "s/[a-z :_']*\([0-9.]*\)'/\1/"` ; \
        echo "$(COLOR_WARNING)" ; read -p "Changez l'IP du Raspberry Pi ? [$$raspi] : " raspi ; \
        if [ ! -z $$raspi ] ; then sed -ri "s/'[0-9.]*'/'`echo $$raspi`'/" hosts.yml ; fi ; echo "$(COLOR_DEFAULT)"
	@echo "$(COLOR_INFO)Lancement de l'installation distante...$(COLOR_DEFAULT)"
	ansible-playbook moodlebox.yml
